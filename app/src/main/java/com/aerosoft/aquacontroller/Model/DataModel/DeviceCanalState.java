package com.aerosoft.aquacontroller.Model.DataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Doc on 14.11.2016.
 */

public class DeviceCanalState implements Cloneable, IValidateData {
    @SerializedName("cl")
    private ArrayList<Integer> canal;

    @SerializedName("c_t")
    private ArrayList<Integer> canal_timer;

    public ArrayList<Integer> getCanal_timer() {
        return canal_timer;
    }

    public ArrayList<Integer> getCanal() {
        return canal;
    }

    public DeviceCanalState clone() throws CloneNotSupportedException {
        DeviceCanalState newDeviceCanalState = (DeviceCanalState) super.clone();
        newDeviceCanalState.canal = (ArrayList<Integer>) canal.clone();
        newDeviceCanalState.canal_timer = (ArrayList<Integer>) canal_timer.clone();
        return newDeviceCanalState;
    }

    @Override
    public boolean IsValidate() {
        if (canal == null || canal_timer == null)
            return false;
        return canal.size() == canal_timer.size();
    }
}
