package com.aerosoft.aquacontroller.Model.DataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DevicePhTimers implements  Cloneable, IValidateData {

    @SerializedName("ph_s")
    private ArrayList<Integer> ph_start;
    @SerializedName("ph_e")
    private ArrayList<Integer> ph_end;
    @SerializedName("ph_st")
    private ArrayList<Integer> ph_state;
    @SerializedName("ph_c")
    private ArrayList<Integer> ph_canal;
    @SerializedName("ph_401")
    private Integer ph_401;
    @SerializedName("ph_686")
    private Integer ph_686;

    public ArrayList<Integer> getPh_start() { return ph_start; }
    public ArrayList<Integer> getPh_end() { return ph_end; }
    public ArrayList<Integer> getPh_state() { return ph_state; }
    public ArrayList<Integer> getPh_canal() { return ph_canal; }
    public Integer getPh_401() { return ph_401; }
    public Integer getPh_686() { return ph_686; }


    public void setPh_canal(ArrayList<Integer> ph_canal) { this.ph_canal = ph_canal; }
    public void setPh_start(ArrayList<Integer> ph_start) { this.ph_start = ph_start; }
    public void setPh_end(ArrayList<Integer> ph_end) { this.ph_end = ph_end; }
    public void setPh_state(ArrayList<Integer> ph_state) { this.ph_state = ph_state; }
    public void setPh_401(Integer ph_401) { this.ph_401 = ph_401; }
    public void setPh_686(Integer ph_686) { this.ph_686 = ph_686; }

    public DevicePhTimers clone() throws CloneNotSupportedException {
        DevicePhTimers newDevicePhTimers = (DevicePhTimers) super.clone();
        newDevicePhTimers.ph_start = (ArrayList<Integer>) ph_start.clone();
        newDevicePhTimers.ph_end = (ArrayList<Integer>) ph_end.clone();
        newDevicePhTimers.ph_state = (ArrayList<Integer>) ph_state.clone();
        newDevicePhTimers.ph_canal = (ArrayList<Integer>) ph_canal.clone();
        newDevicePhTimers.ph_401 =  ph_401;
        newDevicePhTimers.ph_686 =  ph_686;
        return newDevicePhTimers;
    }

    @Override
    public boolean IsValidate() {
        if(ph_start != null && ph_start.size() > 0 &&
                ph_end != null && ph_end.size() > 0 &&
                ph_state != null && ph_state.size() > 0 &&
                ph_canal != null && ph_canal.size() > 0){
         return true;
        }
        return false;
    }



}
