package com.aerosoft.aquacontroller.Model.TaskResponse;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Doc on 14.11.2016.
 */

public class TaskResponse<T extends Cloneable> {
    @SerializedName("status")
    String mStatus;
    @SerializedName("message")
    String message;
    @SerializedName("data")
    T mResponse;

    public String getStatus() {
        return mStatus;
    }

    public String getMessage() {
        return message;
    }

    public T getResponse() {
        if(mResponse == null){
            Log.e("TaskResponse", "mStatus = " + mStatus);
        }
        return mResponse;
    }
}
