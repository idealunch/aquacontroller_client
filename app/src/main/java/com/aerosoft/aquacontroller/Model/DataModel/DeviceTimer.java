package com.aerosoft.aquacontroller.Model.DataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Vadim on 22.11.2016.
 */

public class DeviceTimer implements Cloneable, IValidateData {
    @SerializedName("dt_h_s")
    private ArrayList<Integer> daily_timer_hour_start;
    @SerializedName("dt_h_end")
    private ArrayList<Integer> daily_timer_hour_end;
    @SerializedName("dt_m_s")
    private ArrayList<Integer> daily_timer_min_start;
    @SerializedName("dt_m_e")
    private ArrayList<Integer> daily_timer_min_end;
    @SerializedName("dt_s")
    private ArrayList<Integer> daily_timer_state;
    @SerializedName("dt_c")
    private ArrayList<Integer> daily_timer_canal;


    public ArrayList<Integer> getDaily_timer_hour_start() {
        return daily_timer_hour_start;
    }

    public ArrayList<Integer> getDaily_timer_hour_end() {
        return daily_timer_hour_end;
    }

    public ArrayList<Integer> getDaily_timer_min_start() {
        return daily_timer_min_start;
    }

    public ArrayList<Integer> getDaily_timer_min_end() {
        return daily_timer_min_end;
    }

    public ArrayList<Integer> getDaily_timer_state() {
        return daily_timer_state;
    }

    public ArrayList<Integer> getDaily_timer_canal() {
        return daily_timer_canal;
    }

    public DeviceTimer clone() throws CloneNotSupportedException {
        DeviceTimer newDeviceTimer = (DeviceTimer) super.clone();
        newDeviceTimer.daily_timer_hour_start = (ArrayList<Integer>) daily_timer_hour_start.clone();
        newDeviceTimer.daily_timer_hour_end = (ArrayList<Integer>) daily_timer_hour_end.clone();
        newDeviceTimer.daily_timer_min_start = (ArrayList<Integer>) daily_timer_min_start.clone();
        newDeviceTimer.daily_timer_min_end = (ArrayList<Integer>) daily_timer_min_end.clone();
        newDeviceTimer.daily_timer_state = (ArrayList<Integer>) daily_timer_state.clone();
        newDeviceTimer.daily_timer_canal = (ArrayList<Integer>) daily_timer_canal.clone();

        return newDeviceTimer;
    }

    @Override
    public boolean IsValidate() {
        if (daily_timer_hour_start == null || daily_timer_hour_end == null ||
                daily_timer_min_start == null || daily_timer_min_end == null ||
                daily_timer_state == null || daily_timer_canal == null)
            return false;
        int size = daily_timer_hour_start.size();
        return !(daily_timer_hour_end.size() != size || daily_timer_min_start.size() != size ||
                daily_timer_min_end.size() != size || daily_timer_state.size() != size ||
                daily_timer_canal.size() != size);
    }
}
