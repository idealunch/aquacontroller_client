package com.aerosoft.aquacontroller.Model.DataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DevicePh implements Cloneable, IValidateData {

    @SerializedName("ph")
    private ArrayList<Integer> ph_current;
    @SerializedName("stat")
    private ArrayList<Integer> ph_stats;

    public ArrayList<Integer> getPh_current() {
        return ph_current;
    }

    public void setPh_current(ArrayList<Integer> ph_current) {
        this.ph_current = ph_current;
    }

    public ArrayList<Integer> getPh_stats() {
        return ph_stats;
    }

    public void setPh_stats(ArrayList<Integer> ph_stats) {
        this.ph_stats = ph_stats;
    }


    @Override
    public boolean IsValidate() {

        if (ph_current != null && ph_current.size() > 0 && ph_stats != null && ph_stats.size() > 0)
            return true;
        return false;
    }

    public DevicePh clone() throws CloneNotSupportedException {
        DevicePh devicePh = (DevicePh) super.clone();
        devicePh.ph_current = (ArrayList<Integer>) ph_current.clone();

        devicePh.ph_stats = (ArrayList<Integer>) ph_stats.clone();
        return devicePh;
    }


}
