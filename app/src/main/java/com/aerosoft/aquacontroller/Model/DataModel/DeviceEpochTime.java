package com.aerosoft.aquacontroller.Model.DataModel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DrJar on 12.10.2017.
 */

public class DeviceEpochTime implements Cloneable, IValidateData {
    @SerializedName("epoch")
    private Long epochTime;
    @SerializedName("utc")
    private int utc;
    @Override
    public boolean IsValidate() {

            return true;
        }

    public Long getEpochTime() {
        return epochTime;
    }

    public void setEpochTime(Long epochTime) {
        this.epochTime = epochTime;
    }

    public int getUtc() {
        return utc;
    }

    public void setUtc(int utc) {
        this.utc = utc;
    }
}
