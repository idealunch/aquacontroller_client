package com.aerosoft.aquacontroller.Model.TaskRequest;

import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;

import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.PROTOCOL.INFO;

/**
 * Created by Vadim on 30.01.2017.
 */

public class TaskInfoRequest extends TaskRequest {
    private static final ProtocolHelper.PROTOCOL protocol = INFO;

    public TaskInfoRequest(ProtocolHelper.REQUEST_TYPE request_type, String message) {
        super(request_type, message, protocol);
    }

    @Override
    public void InitializedRequest() {
        request = "{\"status\":\"" + ProtocolHelper.ProtocolList.get(protocol) + "\",\"message\":\"";
        request += ProtocolHelper.CommandList.get(request_type);
        request += "\",\"log\":\"" + jsonObject.toString() + "\"}";

    }
}
