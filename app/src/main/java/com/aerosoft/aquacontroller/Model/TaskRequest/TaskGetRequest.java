package com.aerosoft.aquacontroller.Model.TaskRequest;

import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;

import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.PROTOCOL.GET;

/**
 * Created by Vadim on 30.01.2017.
 */

public class TaskGetRequest extends TaskRequest {
    private static final ProtocolHelper.PROTOCOL protocol = GET;

    public TaskGetRequest(ProtocolHelper.REQUEST_TYPE request_type, Object jsonObject) {
        super(request_type, jsonObject, protocol);
    }

    @Override
    public void InitializedRequest() {
        request = "{\"status\":\"" + ProtocolHelper.ProtocolList.get(protocol) + "\",\"message\":\"";
        request += ProtocolHelper.CommandList.get(request_type);
        request += "\"}";
    }
}
