package com.aerosoft.aquacontroller.Model.DataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeviceTempStats implements Cloneable, IValidateData{

    @SerializedName("sensor0")
    private ArrayList<Integer> sensor1;
    @SerializedName("sensor1")
    private ArrayList<Integer> sensor2;
    @SerializedName("sensor2")
    private ArrayList<Integer> sensor3;
    @SerializedName("sensor3")
    private ArrayList<Integer> sensor4;

    public ArrayList<Integer> getSensor1() {
        return sensor1;
    }

    public void setSensor1(ArrayList<Integer> sensor1) {
        this.sensor1 = sensor1;
    }

    public ArrayList<Integer> getSensor2() {
        return sensor2;
    }

    public void setSensor2(ArrayList<Integer> sensor2) {
        this.sensor2 = sensor2;
    }

    public ArrayList<Integer> getSensor3() {
        return sensor3;
    }

    public void setSensor3(ArrayList<Integer> sensor3) {
        this.sensor3 = sensor3;
    }

    public ArrayList<Integer> getSensor4() {
        return sensor4;
    }

    public void setSensor4(ArrayList<Integer> sensor4) {
        this.sensor4 = sensor4;
    }

    public Map<Integer, List<Integer>> SensorStats(){
        HashMap<Integer, List<Integer>> result = new HashMap<Integer, List<Integer>>();
        result.put(0, sensor1);
        result.put(1, sensor2);
        result.put(2, sensor3);
        result.put(3, sensor4);
        return result;

    }

    @Override
    public boolean IsValidate() {

        if (sensor1 != null && sensor1.size() > 0 &&
                sensor2 != null && sensor2.size() > 0 &&
                sensor3 != null && sensor3.size() > 0 &&
                sensor4 != null && sensor4.size() > 0)
            return true;
        return false;
    }

    public DeviceTempStats clone() throws CloneNotSupportedException {
        DeviceTempStats deviceTempStats = (DeviceTempStats) super.clone();
        deviceTempStats.sensor1 = (ArrayList<Integer>) sensor1.clone();
        deviceTempStats.sensor2 = (ArrayList<Integer>) sensor2.clone();
        deviceTempStats.sensor3 = (ArrayList<Integer>) sensor3.clone();
        deviceTempStats.sensor4 = (ArrayList<Integer>) sensor4.clone();
        return deviceTempStats;
    }

}
