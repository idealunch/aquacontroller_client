package com.aerosoft.aquacontroller.Model.DataModel;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Doc on 09.10.2017.
 */

public class ManagerStats {
    private Context context;
    private Map<Integer, List<Integer>> SensorState;
    private Map<Integer, List<Date>> SensorTime;
    private DeviceTempStats tempStats;

    private boolean isInit = false;


    private static ManagerStats instance;
    private int maxTempSensor;


    public Map<Integer, List<Integer>> getSensorState() {
        return SensorState;
    }

    public DeviceTempStats getDeviceSensorStats() {
        return tempStats;
    }

    public Map<Integer, List<Date>> getSensorTime() {
        return SensorTime;
    }

    private ManagerStats(Context context) {
        this.context = context;
    }

    public static ManagerStats getInstance(Context context) {
        if (instance == null) {
            instance = new ManagerStats(context);
        }
        return instance;
    }

    /**
     * @param deviceInfo
     */
    public void Initialized(DeviceInfo deviceInfo) {
        if (!isInit) {
            this.maxTempSensor = deviceInfo.getMax_temp_sensor();
            SensorState = new HashMap<>();
            SensorTime = new HashMap<>();
            isInit = true;
        }
    }

    public void OnChangeDevice() {
        isInit = false;
    }


    public boolean SetDataTemp(DeviceTempStats tempStats) throws Exception {
        if (!isInit) {
            Log.e(this.getClass().getName(), "Before usage your need initilized");
            return false;
        }
        if (tempStats == null || SensorTime == null) return false;
        this.tempStats = tempStats;
        SensorState = tempStats.SensorStats();
        //TODO 24!!!
        for (int j = 0; j < maxTempSensor; j++) {
            List<Date> date = new ArrayList<>();
            for (int i = 0; i < 24; i++) {

                Calendar cal = new GregorianCalendar();
                cal.add(Calendar.HOUR_OF_DAY, i -23);
                Date currentTime = cal.getTime();
                date.add(currentTime);

            }
            SensorTime.put(j,date);
        }

        return true;
    }

    public int GetLastDataTemp(int sensor) {
        if (!isInit) {
            Log.e(this.getClass().getName(), "Before usage your need initialized");
            return -1;
        }
        if (SensorState.containsKey(sensor)) {
            int lastIndex = SensorState.get(sensor).size() - 1;
            return SensorState.get(sensor).get(lastIndex);
        }
        return -1;
    }

    public List<Date> GetXAxisValueForTemp(int sensor) {
        List<Date> result = new ArrayList<>();
        if (!isInit) {
            Log.e(this.getClass().getName(), "Before usage your need initialized");
            return result;
        }
        result = SensorTime.get(sensor);
        return result;
    }


}
