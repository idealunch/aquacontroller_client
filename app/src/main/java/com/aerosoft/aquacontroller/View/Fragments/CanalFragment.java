package com.aerosoft.aquacontroller.View.Fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.Controller.DataProvider.RequestManager;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceCanalState;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.DeviceActivity;
import com.aerosoft.aquacontroller.View.ViewAdapter.ListCanalsAdapter;
import com.aerosoft.aquacontroller.View.ViewHelper.ToastHelper;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Objects;

import static android.view.View.VISIBLE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DATA_DEVICE_BUSY;


public class CanalFragment extends BaseFragment implements IRequestListener {



    private ListView canalList;
    private DeviceCanalState changedDeviceCanalState;
    private DeviceInfo deviceInfo;
    private DeviceCanalState event;
    private FloatingActionButton fab;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chanal, container, false);
        fab = view.findViewById(R.id.fab_canal);
        fab.setOnClickListener(v -> OnFabClick());
        fab.bringToFront();
        canalList = view.findViewById(R.id.chanalDeviceListView);
        return view;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceInfo deviceInfo) {
        if (deviceInfo != null) {
            this.deviceInfo = deviceInfo;
        }
        if (changedDeviceCanalState == null) {
            try {
                CreateListAdapter();
            } catch (CloneNotSupportedException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceCanalState event) {
        if (event != null) {
            this.event = event;
        }
        if (changedDeviceCanalState == null) {
            try {
                CreateListAdapter();
            } catch (CloneNotSupportedException e1) {
                e1.printStackTrace();
            }
        }

    }



    @Override
    public boolean onCheckValidResult() {
        return event.getCanal_timer().equals(changedDeviceCanalState.getCanal_timer());
    }

    @Override
    public void CreateListAdapter() throws CloneNotSupportedException {
        if (this.event == null || this.deviceInfo == null) return;
        if (snackbar != null)
            snackbar.dismiss();
        ListCanalsAdapter adapter = new ListCanalsAdapter(getActivity(), event.clone(), deviceInfo, e -> {
            if (getActivity() != null && !((DeviceActivity) getActivity()).isWait)
                SetFabVisible(View.VISIBLE);
            //cloneable object
            changedDeviceCanalState = e;
        });
        canalList.setAdapter(adapter);
    }

    @Override
    public void OnFabClick() {
        if (!((DeviceActivity) Objects.requireNonNull(getActivity())).isWait) {
            ((DeviceActivity) getActivity()).isWait = true;
            snackbar.show();
            if (changedDeviceCanalState != null) {
                attemptCount = com.aerosoft.aquacontroller.Controller.DataHelper.Constants.ATTEMPT_COUNT;
                new RequestManager().SendPOSTRequest(REQUEST_TYPE.REQUEST_DEVICE_CANAL_STATE, changedDeviceCanalState, CanalFragment.this);
            }
        } else {
            ToastHelper.ToastDeviceLog(getActivity(), RESULT_DATA_DEVICE_BUSY);
        }
    }

    @Override
    public void SendPostData() {
        new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_CANAL_STATE, changedDeviceCanalState, CanalFragment.this);
    }

    @Override
    public void ClearTempData() {
        changedDeviceCanalState = null;
    }

    @Override
    public void SetFabVisible(int visible) {
        if (fab != null) {
            if (visible == VISIBLE)
                fab.show();
            else
                fab.hide();
        }
    }
}
