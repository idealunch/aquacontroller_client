package com.aerosoft.aquacontroller.View.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;

import com.aerosoft.aquacontroller.Controller.DataHelper.Constants;
import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.DeviceActivity;
import com.aerosoft.aquacontroller.View.Fragments.Base.IBaseFragment;
import com.aerosoft.aquacontroller.View.ViewHelper.ToastHelper;

import org.greenrobot.eventbus.EventBus;

import java.util.EventObject;
import java.util.Objects;

import static android.view.View.INVISIBLE;

public abstract class BaseFragment extends Fragment implements IBaseFragment, IRequestListener {
    protected PHFragment.OnFragmentInteractionListener mListener;
    protected Snackbar snackbar;
    protected int attemptCount = Constants.ATTEMPT_COUNT;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        snackbar = Snackbar.make(Objects.requireNonNull(getActivity()).findViewById(android.R.id.content), getString(R.string.SendDataMessage), Snackbar.LENGTH_INDEFINITE)
                .setAction("Action", null);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseFragment.OnFragmentInteractionListener) {
            mListener = (BaseFragment.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        SetFabVisible(INVISIBLE);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        ClearTempData();
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onEvent(EventObject e) {
        if (e == null || e.getSource() != ProtocolHelper.TaskResult.RESULT_SUCCESS || !onCheckValidResult()) {
            attemptCount--;
            if (attemptCount > 0)
                SendPostData();
            else {
                IsWait(false);
                if(snackbar != null){
                    snackbar.dismiss();
                }
                SetFabVisible(View.VISIBLE);
                ToastHelper.ToastDeviceLog(e);
            }
        } else {
            try {
                CreateListAdapter();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            if(snackbar != null){
                snackbar.dismiss();
            }
            IsWait(false);
            ToastHelper.ToastDeviceLog(e);
            ClearTempData();
        }
    }

    public boolean IsWait() {
        if (getActivity() != null)
            return ((DeviceActivity) getActivity()).isWait;
        return false;
    }

    public void IsWait(boolean isWait) {
        if (getActivity() != null)
            ((DeviceActivity) getActivity()).isWait = isWait;
    }

    public abstract boolean onCheckValidResult();

    public abstract void CreateListAdapter() throws CloneNotSupportedException;

    public abstract void OnFabClick();

    public abstract void SendPostData();

    public abstract void ClearTempData();

    public abstract void SetFabVisible(int visible);
}
