package com.aerosoft.aquacontroller.View.ViewAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.aerosoft.aquacontroller.View.Fragments.timers.DailyFragment;
import com.aerosoft.aquacontroller.View.Fragments.timers.HourFragment;
import com.aerosoft.aquacontroller.View.Fragments.timers.SecondFragment;


/**
 * Created by Vadim on 01.12.2016.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new DailyFragment();
            case 1:
                return new HourFragment();
            case 2:
                return new SecondFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
