package com.aerosoft.aquacontroller.View.Fragments.Base;

public interface IBaseFragment {
    boolean onCheckValidResult();
    void CreateListAdapter() throws CloneNotSupportedException;
    void OnFabClick();
    void SendPostData();
    void ClearTempData();
    void SetFabVisible(int visible);
}
