package com.aerosoft.aquacontroller.View.ViewHelper;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempState;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempStats;

import org.jetbrains.annotations.NotNull;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;

/**
 * Created by DrJar on 16.10.2017.
 */

public class Helpers {

    public static Bitmap textAsBitmap(String text, float textSize, int textColor) {
        Paint paint = new Paint(ANTI_ALIAS_FLAG);
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.5f); // round
        int height = (int) (baseline + paint.descent() + 0.5f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }

    public static boolean IsCanShowSensorTemp(@NotNull DeviceInfo deviceInfo, @NotNull DeviceTempStats statsTempSensor, int sensor){
        if (sensor > deviceInfo.getMax_temp_sensor())
            return false;
        int res = 0;
        int count = statsTempSensor.SensorStats().get(sensor).size();
        for (int i = 0; i < count; i++) {
            res +=statsTempSensor.SensorStats().get(sensor).get(i);
        }
        if(res > 0) return true;
        return false;
    }

    public static boolean IsCanShowTempStatistics(@NotNull DeviceInfo deviceInfo){
        return deviceInfo.getVersion().equals(ProtocolHelper.DEVICE_TYPE_ENUM.AQ_CH08WP.toString());
    }
}
