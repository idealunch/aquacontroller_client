package com.aerosoft.aquacontroller.View;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.Toast;

import com.aerosoft.aquacontroller.Controller.DataHelper.ConnectionHelper;
import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Controller.DataHelper.UdpHelper;
import com.aerosoft.aquacontroller.Controller.DataListener.IDataIPEventListener;
import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.Controller.DataProvider.RequestManager;
import com.aerosoft.aquacontroller.Controller.DataProvider.UDPBackGroundService;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempStats;
import com.aerosoft.aquacontroller.Model.DataModel.ManagerStats;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.Fragments.Preference.DialogPreferences;
import com.aerosoft.aquacontroller.View.Fragments.Preference.ShareManager;
import com.aerosoft.aquacontroller.View.ViewAdapter.ListIPAdapter;
import com.aerosoft.aquacontroller.View.ViewHelper.ToastHelper;
import com.eftimoff.androipathview.PathView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.EventObject;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_CANAL_STATE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_DAILY_TIMER_SATE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_HOURS_TIMER_SATE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_INFO;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_PH;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_PH_TIMER;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_SECOND_TIMER_SATE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_TEMP_SENSOR;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_TEMP_STATE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_TEMP_STATS;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_CONNECTION;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_CHANAL_STATE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_INFO;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_PH;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_PH_TIMER;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_TEMP;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_TEMP_SENSOR;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_TEMP_STATS;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_TIME_DAILY;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_TIME_HOUR;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_TIME_SECOND;


public class SplashActivity extends AppCompatActivity implements IDataIPEventListener {


    private SplashActivity currentActivity;
    private int attemptCount = com.aerosoft.aquacontroller.Controller.DataHelper.Constants.ATTEMPT_COUNT;
    private boolean isConnected = false;
    private Snackbar snackbar;
    private FloatingActionButton fab;
    private ImageButton settingsButton;
    private PathView[] pathViewImage;
    private int indexTaskQuery = 0;
    private int indexTaskPathView = 0;
    private RecyclerView listIp;
    private ProtocolHelper.REQUEST_TYPE[] taskProtocol = {
            REQUEST_DEVICE_INFO,
            REQUEST_DEVICE_CANAL_STATE,
            REQUEST_DEVICE_DAILY_TIMER_SATE,
            REQUEST_DEVICE_HOURS_TIMER_SATE,
            REQUEST_DEVICE_SECOND_TIMER_SATE,
            REQUEST_DEVICE_TEMP_STATE,
            REQUEST_DEVICE_TEMP_SENSOR,
            REQUEST_DEVICE_PH,
            REQUEST_DEVICE_PH_TIMER,
            REQUEST_DEVICE_TEMP_STATS};

    private TaskResult[] taskQuery = {
            RESULT_DEVICE_INFO,
            RESULT_DEVICE_CHANAL_STATE,
            RESULT_DEVICE_TIME_DAILY,
            RESULT_DEVICE_TIME_HOUR,
            RESULT_DEVICE_TIME_SECOND,
            RESULT_DEVICE_TEMP,
            RESULT_DEVICE_TEMP_SENSOR,
            RESULT_DEVICE_PH,
            RESULT_DEVICE_PH_TIMER,
            RESULT_DEVICE_TEMP_STATS};
    private DeviceInfo deviceInfo;
    private ListIPAdapter adapterIPList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        currentActivity = this;
        super.onCreate(savedInstanceState);
        initStatusBar();
        setContentView(R.layout.activity_main);

        ToastHelper.Initilized(getApplicationContext());
        pathViewImage = new PathView[]{
                findViewById(R.id.deviceConnectPathView),
                findViewById(R.id.deviceInfoPathView),
                findViewById(R.id.deviceChanalPathView),
                findViewById(R.id.deviceTimerPathView),
                findViewById(R.id.deviceTimerHoursPathView),
                findViewById(R.id.deviceTimerSecondPathView),
                findViewById(R.id.deviceTempPathView),
                findViewById(R.id.deviceRealTempPathView),
                findViewById(R.id.deviceTempStatsPathView),
                findViewById(R.id.devicePhPathView)};

        fab = findViewById(R.id.fabSplash);
        listIp = findViewById(R.id.ip_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        listIp.setLayoutManager(linearLayoutManager);
        settingsButton = findViewById(R.id.imageButton);
        settingsButton.setOnClickListener(view -> {
            Intent settingsActivity = new Intent(getBaseContext(),
                    DialogPreferences.class);
            startActivity(settingsActivity);
        });
        fab.hide();
        fab.setOnClickListener(v -> {
            Snackbar.make(v, getString(R.string.dialog_try_again), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            fab.hide();
            settingsButton.setVisibility(INVISIBLE);
            indexTaskPathView = 0;
            indexTaskQuery = 0;
            indexTaskPathView = 0;
            attemptCount = com.aerosoft.aquacontroller.Controller.DataHelper.Constants.ATTEMPT_COUNT;
            for (PathView image : pathViewImage) {
                image.setPercentage(0);
            }
            CheckAccessDevice();
        });
        adapterIPList = new ListIPAdapter(this, this);
        listIp.setAdapter(adapterIPList);
        Intent intentService = new Intent(this, UDPBackGroundService.class);
        startService(intentService);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        for (PathView image : pathViewImage) {
            image.setPercentage(0);
        }
        if (ShareManager.getInstance(this).GetAllIP().size() > 0) {
            adapterIPList = new ListIPAdapter(this, this);
            listIp.setAdapter(adapterIPList);
        }
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void CheckAccessDevice() {
        if (!UdpHelper.ADDRESS.isEmpty()) {
            new ConnectionHelper().getConnectedDevices(e -> {
                if (e.getSource() == TaskResult.RESULT_SUCCESS) {
                    isConnected = true;
                    StartAnimation(RESULT_CONNECTION);
                } else {
                    attemptCount--;
                    if (attemptCount < 0) {
                        if (indexTaskPathView > 0)
                            indexTaskPathView--;
                        fab.show();
                        settingsButton.setVisibility(VISIBLE);
                        Toast.makeText(getApplicationContext(), ProtocolHelper.GetMessageToast(currentActivity, (TaskResult) (e.getSource())),
                                Toast.LENGTH_LONG).show();

                    } else {
                        CheckAccessDevice();
                    }
                }

            });
        } else {
            /*Intent settingsActivity = new Intent(getBaseContext(),
                    DialogPreferences.class);
            startActivity(settingsActivity);*/

        }
    }

    private void StartActivity() {
        Intent intent = new Intent(currentActivity, DeviceActivity.class);
        startActivity(intent);
        currentActivity.finish();
    }


    private void GetDataController() {

        new RequestManager().SendGETRequest(taskProtocol[indexTaskQuery], new IRequestListener() {
            @Override
            public void onEvent(EventObject e) {
                if (e != null && e.getSource() == TaskResult.RESULT_SUCCESS) {

                    if (taskQuery.length > indexTaskQuery) {
                        StartAnimation(taskQuery[indexTaskQuery]);
                        indexTaskQuery++;
                    }
                } else {
                    attemptCount--;
                    if (attemptCount < 0) {
                        fab.show();
                        settingsButton.setVisibility(VISIBLE);
                        if (e != null) {
                            Log.e("GetDataController", e.getSource().toString());
                            ToastHelper.SnackBarShow(findViewById(R.id.main_activity_id), e.getSource().toString());
                        } else {
                            ToastHelper.SnackBarShow(findViewById(R.id.main_activity_id), TaskResult.RESULT_DATA_CORRUPTED);
                        }
                    } else {
                        GetDataController();
                    }
                }
            }
        });
    }

    private void StartAnimation(final TaskResult task) {
        final int delay = 200;
        int duration = 200;
        attemptCount = com.aerosoft.aquacontroller.Controller.DataHelper.Constants.ATTEMPT_COUNT;
        pathViewImage[indexTaskPathView].getPathAnimator().delay(delay).duration(duration).listenerEnd(new PathView.AnimatorBuilder.ListenerEnd() {
            @Override
            public void onAnimationEnd() {
                if (task != RESULT_DEVICE_PH) {
                    indexTaskPathView++;
                }
                switch (task) {
                    case RESULT_INIT_LOGO:
                        CheckAccessDevice();
                        break;
                    case RESULT_DEVICE_TEMP_SENSOR:
                        if (deviceInfo.getVersion().equals(ProtocolHelper.DEVICE_TYPE_ENUM.AQ_CH08W.toString())) {
                            if (isConnected) {
                                StartActivity();
                            }
                        } else {
                            GetDataController();
                        }
                        break;
                    case RESULT_DEVICE_TEMP_STATS:
                        if (isConnected) {
                            StartActivity();
                        }
                        break;
                    default:
                        if (isConnected) {
                            GetDataController();
                        }
                        break;
                }

            }
        }).interpolator(new AccelerateDecelerateInterpolator()).start();


    }


    @Override
    public void Event() {
        adapterIPList = new ListIPAdapter(this, this);
        listIp.setAdapter(adapterIPList);
    }

    @Override
    public void Event(String ip) {

        indexTaskPathView = 0;
        indexTaskQuery = 0;
        indexTaskPathView = 0;
        attemptCount = com.aerosoft.aquacontroller.Controller.DataHelper.Constants.ATTEMPT_COUNT;
        for (PathView image : pathViewImage) {
            image.setPercentage(0);
        }
        UdpHelper.ADDRESS = ip;
        ShareManager.getInstance(this).Initilized();
        CheckAccessDevice();
        fab.hide();
        settingsButton.setVisibility(INVISIBLE);

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceInfo deviceInfo) {
        if (deviceInfo != null) {
            adapterIPList = new ListIPAdapter(this, this);
            listIp.setAdapter(adapterIPList);
            listIp.invalidate();
            this.deviceInfo = deviceInfo;
            ManagerStats.getInstance(this).Initialized(deviceInfo);
        }

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceTempStats tempSensor) {
        try {
            ManagerStats.getInstance(this).SetDataTemp(tempSensor);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initStatusBar() {
        Window window = getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            runOnUiThread(() -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    View decor = getWindow().getDecorView();
                    decor.setSystemUiVisibility(addHideNavigationFlags(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR));
                }
            });
        }

    }

    private static int addHideNavigationFlags(int flags) {
        return flags | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
    }


}
