package com.aerosoft.aquacontroller.View.Fragments.timers;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.Fragments.BaseFragment;
import com.aerosoft.aquacontroller.View.ViewAdapter.PagerAdapter;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;


public class TimersFragment extends BaseFragment {


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = Objects.requireNonNull(inflater).inflate(R.layout.fragment_timers, container, false);
        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.tab_item_daily)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.tab_item_hours)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.tab_item_second)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ViewPager viewPager = view.findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (Objects.requireNonNull(getActivity()).getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setOffscreenPageLimit(tabLayout.getTabCount());
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        return view;
    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceInfo deviceInfo) {    }

    @Override
    public boolean onCheckValidResult() {
        return false;
    }

    @Override
    public void CreateListAdapter() throws CloneNotSupportedException {    }

    @Override
    public void OnFabClick() {    }

    @Override
    public void SendPostData() {    }

    @Override
    public void ClearTempData() {    }

    @Override
    public void SetFabVisible(int visible) {    }

}
