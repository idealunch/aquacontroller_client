package com.aerosoft.aquacontroller.View.Fragments.timers;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aerosoft.aquacontroller.Controller.DataHelper.Constants;
import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.Controller.DataProvider.RequestManager;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTimer;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.Fragments.BaseFragment;
import com.aerosoft.aquacontroller.View.ViewAdapter.TimerDailyListAdapter;
import com.aerosoft.aquacontroller.View.ViewHelper.ToastHelper;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DATA_DEVICE_BUSY;

public class DailyFragment extends BaseFragment implements IRequestListener {

    RecyclerView mRecyclerView;
    private DeviceInfo deviceInfo;
    private DeviceTimer event;
    private DeviceTimer changedDeviceTimer;
    private FloatingActionButton fab;
    private TimerDailyListAdapter adapter;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timers_daily, container, false);
        mRecyclerView = view.findViewById(R.id.recycler_view_daily);
        fab = view.findViewById(R.id.fab_daily);
        fab.bringToFront();
        fab.setOnClickListener(v -> OnFabClick());
        return view;
    }

    @Override
    public boolean onCheckValidResult() {
        if (event == null) {
            return false;
        }
        if (event.getDaily_timer_canal() == null || !this.event.getDaily_timer_canal().equals(changedDeviceTimer.getDaily_timer_canal()))
            return false;
        if (event.getDaily_timer_hour_end() == null || !this.event.getDaily_timer_hour_end().equals(changedDeviceTimer.getDaily_timer_hour_end()))
            return false;
        if (event.getDaily_timer_hour_end() == null || !this.event.getDaily_timer_hour_start().equals(changedDeviceTimer.getDaily_timer_hour_start()))
            return false;
        if (event.getDaily_timer_min_end() == null || !this.event.getDaily_timer_min_end().equals(changedDeviceTimer.getDaily_timer_min_end()))
            return false;
        if (event.getDaily_timer_min_start() == null || !this.event.getDaily_timer_min_start().equals(changedDeviceTimer.getDaily_timer_min_start()))
            return false;
        return this.event.getDaily_timer_state().equals(changedDeviceTimer.getDaily_timer_state());
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceInfo deviceInfo) {
        if (deviceInfo != null) {
            this.deviceInfo = deviceInfo;
            if (this.event != null) {
                if (changedDeviceTimer == null)
                    CreateListAdapter();
            }
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceTimer event) {
        if (event != null) {
            this.event = event;
            if (this.deviceInfo != null)
                if (changedDeviceTimer == null)
                    CreateListAdapter();
        }
    }

    @Override
    public void CreateListAdapter() {
        try {
            if (snackbar != null)
                snackbar.dismiss();
            adapter = GetAdapter();
            mRecyclerView.setAdapter(adapter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mRecyclerView.invalidate();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void OnFabClick() {
        attemptCount = Constants.ATTEMPT_COUNT;
        if (!IsWait()) {
            IsWait(true);
            snackbar.show();
            SetFabVisible(INVISIBLE);
            if (changedDeviceTimer != null) {
                SendPostData();
            }
        } else {
            ToastHelper.ToastDeviceLog(getActivity(), RESULT_DATA_DEVICE_BUSY);
        }
    }

    @Override
    public void SendPostData() {
        new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_DAILY_TIMER_SATE, changedDeviceTimer, DailyFragment.this);
    }

    @Override
    public void ClearTempData() {
        changedDeviceTimer = null;
    }

    @Override
    public void SetFabVisible(int visible) {
        if (fab != null) {
            if (visible == VISIBLE)
                fab.show();
            else
                fab.hide();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            SetFabVisible(INVISIBLE);
            if (adapter != null) {
                adapter = GetAdapter();
                mRecyclerView.setAdapter(adapter);
            }
        }
    }

    private TimerDailyListAdapter GetAdapter() {
        return new TimerDailyListAdapter(getContext(), event, this.deviceInfo, e -> {
            changedDeviceTimer = e;
            if (getActivity() != null && !IsWait())
                SetFabVisible(View.VISIBLE);
        });
    }
}
