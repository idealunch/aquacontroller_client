package com.aerosoft.aquacontroller.View.ViewAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.Fragments.Preference.ShareManager;

/**
 * Created by Vadim on 29.11.2016.
 */

public class SpinnerAdapter extends BaseAdapter {

    private Context context;
    private int[] list;
    private int maxChannel;

    public SpinnerAdapter(Context context, int[] list, int maxChannel){
        this.context = context;
        this.list = list;
        this.maxChannel = maxChannel;
    }
    @Override
    public int getCount() {
        return maxChannel;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.spinner_layout, parent, false);
        ImageView imageView = layout.findViewById(R.id.spinner_chanal_image);
        imageView.setImageResource(list[position]);
        return layout;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.spinner_item, parent, false);
        ImageView imageView = layout.findViewById(R.id.spinner_chanal_image);
        TextView canalName = layout.findViewById(R.id.canalNameSpinner);
        canalName.setText(ShareManager.getInstance(context).GetPowerCanalName(position));
        imageView.setImageResource(list[position]);
        return layout;
    }
}
