package com.aerosoft.aquacontroller.View.Fragments.timers;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aerosoft.aquacontroller.Controller.DataHelper.Constants;
import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.Controller.DataProvider.RequestManager;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceHoursTimer;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.Fragments.BaseFragment;
import com.aerosoft.aquacontroller.View.ViewAdapter.TimerHoursListAdapter;
import com.aerosoft.aquacontroller.View.ViewHelper.ToastHelper;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DATA_DEVICE_BUSY;

public class HourFragment extends BaseFragment implements IRequestListener {


    RecyclerView mRecyclerView;
    private DeviceHoursTimer event;
    private DeviceInfo deviceInfo;
    private DeviceHoursTimer changedDeviceHoursTimer;
    private FloatingActionButton fab;
    private TimerHoursListAdapter adapter;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timers_hours, container, false);
        mRecyclerView = view.findViewById(R.id.recycler_view_hours);
        fab = view.findViewById(R.id.fab_hours);
        fab.setOnClickListener(v -> OnFabClick());
        fab.bringToFront();
        return view;
    }


    @Override
    public boolean onCheckValidResult() {
        if (!this.event.getHours_timer_canal().equals(changedDeviceHoursTimer.getHours_timer_canal()))
            return false;
        if (!this.event.getHours_timer_min_start().equals(changedDeviceHoursTimer.getHours_timer_min_start()))
            return false;
        if (!this.event.getHours_timer_min_stop().equals(changedDeviceHoursTimer.getHours_timer_min_stop()))
            return false;
        return this.event.getHours_timer_state().equals(changedDeviceHoursTimer.getHours_timer_state());
    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceInfo deviceInfo) {
        if (deviceInfo != null) {
            this.deviceInfo = deviceInfo;
            if (this.event != null) {
                if (changedDeviceHoursTimer == null) {
                    CreateListAdapter();
                }
            }
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceHoursTimer event) {
        if (event != null) {
            this.event = event;
            if (this.deviceInfo != null) {
                if (changedDeviceHoursTimer == null) {
                    CreateListAdapter();
                }
            }
        }

    }

    @Override
    public void CreateListAdapter() {
        try {
            if (snackbar != null)
                snackbar.dismiss();
            adapter = GetAdapter();
            mRecyclerView.setAdapter(adapter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void OnFabClick() {
        attemptCount = Constants.ATTEMPT_COUNT;
        if (!IsWait()) {
            IsWait(true);
            snackbar.show();
            SetFabVisible(INVISIBLE);
            if (changedDeviceHoursTimer != null) {
                SendPostData();
            }
        } else {
            ToastHelper.ToastDeviceLog(getActivity(), RESULT_DATA_DEVICE_BUSY);
        }
    }

    @Override
    public void SendPostData() {
        new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_HOURS_TIMER_SATE, changedDeviceHoursTimer, HourFragment.this);
    }

    @Override
    public void ClearTempData() {
        changedDeviceHoursTimer = null;
    }

    @Override
    public void SetFabVisible(int visible) {
        if (fab != null) {
            if (visible == VISIBLE)
                fab.show();
            else
                fab.hide();
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            SetFabVisible(INVISIBLE);
            changedDeviceHoursTimer = null;
            if (adapter != null && mRecyclerView != null){
                adapter = GetAdapter();
            mRecyclerView.setAdapter(adapter);}
        }
    }

    private TimerHoursListAdapter GetAdapter() {
        return new TimerHoursListAdapter(getContext(), event, this.deviceInfo, e -> {
            changedDeviceHoursTimer = e;
            if (getActivity() != null && !IsWait())
                SetFabVisible(View.VISIBLE);
        });
    }
}
