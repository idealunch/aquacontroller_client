package com.aerosoft.aquacontroller.View.Fragments.timers;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aerosoft.aquacontroller.Controller.DataHelper.Constants;
import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.Controller.DataProvider.RequestManager;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceSecondsTimer;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.Fragments.BaseFragment;
import com.aerosoft.aquacontroller.View.ViewAdapter.TimerSecondListAdapter;
import com.aerosoft.aquacontroller.View.ViewHelper.ToastHelper;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DATA_DEVICE_BUSY;

public class SecondFragment extends BaseFragment implements IRequestListener {
    RecyclerView mRecyclerView;
    private DeviceSecondsTimer event;
    private DeviceInfo deviceInfo;
    private DeviceSecondsTimer changedDeviceSecondsTimer;
    private FloatingActionButton fab;
    private TimerSecondListAdapter adapter;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = Objects.requireNonNull(inflater).inflate(R.layout.fragment_timers_second, container, false);
        mRecyclerView = view.findViewById(R.id.recycler_view_second);
        fab = view.findViewById(R.id.fab_second);
        fab.bringToFront();
        fab.setOnClickListener(v -> OnFabClick());
        return view;
    }

    @Override
    public boolean onCheckValidResult() {
        if (!this.event.getSecond_timer_canal().equals(changedDeviceSecondsTimer.getSecond_timer_canal()))
            return false;
        if (!this.event.getSecond_timer_duration().equals(changedDeviceSecondsTimer.getSecond_timer_duration()))
            return false;
        if (!this.event.getSecond_timer_hour_start().equals(changedDeviceSecondsTimer.getSecond_timer_hour_start()))
            return false;
        if (!this.event.getSecond_timer_min_start().equals(changedDeviceSecondsTimer.getSecond_timer_min_start()))
            return false;
        return this.event.getSecond_timer_state().equals(changedDeviceSecondsTimer.getSecond_timer_state());
    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceInfo deviceInfo) {
        if (deviceInfo != null) {
            this.deviceInfo = deviceInfo;
            if (this.event != null) {
                if (changedDeviceSecondsTimer == null) {
                    CreateListAdapter();
                }
            }
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceSecondsTimer event) {
        if (event != null) {
            this.event = event;
            if (this.deviceInfo != null) {
                if (changedDeviceSecondsTimer == null) {
                    CreateListAdapter();
                }
            }
        }
    }

    @Override
    public void CreateListAdapter() {
        try {
            if (snackbar != null)
                snackbar.dismiss();
            adapter = GetAdapter();
            mRecyclerView.setAdapter(adapter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mRecyclerView.invalidate();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void OnFabClick() {
        attemptCount = Constants.ATTEMPT_COUNT;
        if (!IsWait()) {
            IsWait(true);
            snackbar.show();
            SetFabVisible(INVISIBLE);
            if (changedDeviceSecondsTimer != null) {
                SendPostData();
            } else {
                ToastHelper.ToastDeviceLog(getActivity(), RESULT_DATA_DEVICE_BUSY);
            }
        }
    }

    @Override
    public void SendPostData() {
        new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_SECOND_TIMER_SATE, changedDeviceSecondsTimer, SecondFragment.this);
    }

    @Override
    public void ClearTempData() {
        changedDeviceSecondsTimer = null;
    }

    @Override
    public void SetFabVisible(int visible) {
        if (fab != null) {
            if (visible == VISIBLE)
                fab.show();
            else
                fab.hide();
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            SetFabVisible(INVISIBLE);
            if(adapter != null  && mRecyclerView != null){
                adapter = GetAdapter();
            mRecyclerView.setAdapter(adapter);}
        }
    }

    private TimerSecondListAdapter GetAdapter(){
        return new TimerSecondListAdapter(getContext(), event, this.deviceInfo, e -> {
            changedDeviceSecondsTimer = e;
            if (getActivity() != null && !IsWait())
            SetFabVisible(VISIBLE);

        });
    }
}
