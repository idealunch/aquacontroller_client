package com.aerosoft.aquacontroller.View.Fragments.Preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.aerosoft.aquacontroller.Controller.DataHelper.Constants;
import com.aerosoft.aquacontroller.R;

public class DialogPreferences extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName("IP");
        addPreferencesFromResource(R.xml.ip_preference);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences preferenceIp = getApplication().getSharedPreferences(Constants.IP_PREF, Context.MODE_PRIVATE);
        ShareManager.getInstance(getApplicationContext()).AddIPDevices(preferenceIp.getString("ip_address",""));

    }


}
