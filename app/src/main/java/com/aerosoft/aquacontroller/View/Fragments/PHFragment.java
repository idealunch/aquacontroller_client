package com.aerosoft.aquacontroller.View.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aerosoft.aquacontroller.Controller.Chart.ChartController;
import com.aerosoft.aquacontroller.Controller.Chart.HelperLineChartView;
import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.Controller.DataProvider.RequestManager;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DevicePh;
import com.aerosoft.aquacontroller.Model.DataModel.DevicePhTimers;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.DeviceActivity;
import com.aerosoft.aquacontroller.View.ViewAdapter.TimerPHAdapter;
import com.aerosoft.aquacontroller.View.ViewHelper.ToastHelper;
import com.github.mikephil.charting.charts.LineChart;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static android.view.View.INVISIBLE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DATA_DEVICE_BUSY;


@SuppressLint("RestrictedApi")
public class PHFragment extends BaseFragment implements IRequestListener {

    private TextView currentPHText;
    private RecyclerView settingList;
    private CheckBox settingCheckBox;
    private LinearLayout settingsLayout;
    private TextView settings4_01Edit;
    private TextView settings6_86Edit;
    private DeviceInfo deviceInfo;
    private DevicePhTimers phTimers;
    private DevicePhTimers phTimersTemp;
    private FloatingActionButton fab;
    private LineChart lineChart;



    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ph, container, false);
        currentPHText = view.findViewById(R.id.phText);
        settingList = view.findViewById(R.id.list_ph);
        settingCheckBox = view.findViewById(R.id.ph_checkBox);
        settingsLayout = view.findViewById(R.id.settings_layout);
        Button settings4_01Button = view.findViewById(R.id.ph4_01);
        Button setting6_86Button = view.findViewById(R.id.ph6_86);
        settings4_01Edit = view.findViewById(R.id.ph_text_config_401);
        settings6_86Edit = view.findViewById(R.id.ph_text_config_686);
        settingCheckBox.setChecked(false);
        settingsLayout.setVisibility(View.GONE);
        lineChart = view.findViewById(R.id.chart_ph);
        fab = view.findViewById(R.id.fab_ph);
        fab.bringToFront();
        fab.setOnClickListener(v -> OnFabClick());
        settingCheckBox.setOnClickListener(v -> {
            if (settingCheckBox.isChecked())
                settingsLayout.setVisibility(View.VISIBLE);
            else
                settingsLayout.setVisibility(View.GONE);
        });
        settings4_01Button.setOnClickListener(v -> {

            if (((DeviceActivity) Objects.requireNonNull(getActivity())).isWait) {
                ToastHelper.ToastDeviceLog(getActivity(), RESULT_DATA_DEVICE_BUSY);
            } else {
                ((DeviceActivity) getActivity()).isWait = true;

                if (phTimers != null) {
                    snackbar = Snackbar.make(v, getString(R.string.SendDataMessage), Snackbar.LENGTH_INDEFINITE)
                            .setAction("Action", null);
                    snackbar.show();
                    SetFabVisible(INVISIBLE);
                    phTimers.setPh_401(0);
                    new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_PH_TIMER, phTimers, PHFragment.this);
                }
            }
        });

        setting6_86Button.setOnClickListener(v -> {
            if (((DeviceActivity) Objects.requireNonNull(getActivity())).isWait) {
                ToastHelper.ToastDeviceLog(getActivity(), RESULT_DATA_DEVICE_BUSY);
            } else {
                ((DeviceActivity) getActivity()).isWait = true;
                if (phTimers != null) {
                    snackbar = Snackbar.make(v, getString(R.string.SendDataMessage), Snackbar.LENGTH_INDEFINITE)
                            .setAction("Action", null);
                    snackbar.show();
                    SetFabVisible(INVISIBLE);
                    phTimers.setPh_686(0);
                    new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_PH_TIMER, phTimers, PHFragment.this);
                }
            }
        });
        return view;
    }

    public boolean onCheckValidResult() {
        if (!phTimers.getPh_start().equals(phTimersTemp.getPh_start()))
            return false;
        if (!phTimers.getPh_end().equals(phTimersTemp.getPh_end()))
            return false;
        if (!phTimers.getPh_state().equals(phTimersTemp.getPh_state()))
            return false;
        return phTimers.getPh_canal().equals(phTimersTemp.getPh_canal());
    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceInfo deviceInfo) {
        if (deviceInfo != null) {
            this.deviceInfo = deviceInfo;
            if (phTimersTemp == null && phTimers != null) {
                try {
                    CreateListAdapter();
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DevicePh currentPh) {
        float ph = (currentPh.getPh_current().get(0) * 10 + deviceInfo.getMin_ph()) / 100f;
        currentPHText.setText(String.format(Locale.ENGLISH, "%02.02f°", ph));
        ChartController chartController = new ChartController(getActivity(), deviceInfo, currentPh);
        HelperLineChartView.Builder builder = new HelperLineChartView.Builder(new LineChart[]{lineChart}, HelperLineChartView.ChartView.CHART_VIEW_LAYOUT, getActivity());
        builder.SetChatLineDataSet(new List[]{chartController.GetEntriesForPh()}, new int[]{R.drawable.fade_layout}, 0);
        builder.SetChatView();
        builder.SetXAxis();
        builder.SetYAxis();
        builder.Build();

    }

    @SuppressLint("SetTextI18n")
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DevicePhTimers devicePhTimers) {
        phTimers = devicePhTimers;
        settings4_01Edit.setText(settings4_01Edit.getText() + phTimers.getPh_401().toString());
        settings6_86Edit.setText(settings6_86Edit.getText() + phTimers.getPh_686().toString());
        try {
            CreateListAdapter();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

    }

    public void CreateListAdapter() throws CloneNotSupportedException {
        if (phTimers == null || deviceInfo == null) return;
        if (snackbar != null)
            snackbar.dismiss();

        TimerPHAdapter adapter = new TimerPHAdapter(getContext(), phTimers.clone(), deviceInfo, e -> {
            SetFabVisible(View.VISIBLE);
            phTimersTemp = e;
        });
        settingList.setAdapter(adapter);
        settingList.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void OnFabClick() {
        if (((DeviceActivity) Objects.requireNonNull(getActivity())).isWait) {
            ToastHelper.ToastDeviceLog(getActivity(), RESULT_DATA_DEVICE_BUSY);
        } else {
            ((DeviceActivity) getActivity()).isWait = true;
            snackbar.show();
            SetFabVisible(INVISIBLE);
            if (phTimersTemp != null) {
                new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_PH_TIMER, phTimersTemp, PHFragment.this);
            }
        }
    }

    @Override
    public void SendPostData() {
        new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_PH_TIMER, phTimersTemp, PHFragment.this);
    }

    @Override
    public void ClearTempData() {
        phTimersTemp = null;
    }

    @Override
    public void SetFabVisible(int visible) {
        fab.setVisibility(visible);
    }


}
