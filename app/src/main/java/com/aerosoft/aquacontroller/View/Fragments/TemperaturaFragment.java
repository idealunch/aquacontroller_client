package com.aerosoft.aquacontroller.View.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.Controller.DataProvider.RequestManager;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempSensors;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempState;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.DeviceActivity;
import com.aerosoft.aquacontroller.View.ViewAdapter.TempStateAdapter;
import com.aerosoft.aquacontroller.View.ViewHelper.ToastHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.EventObject;
import java.util.Objects;

import static android.view.View.INVISIBLE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DATA_DEVICE_BUSY;



public class TemperaturaFragment extends BaseFragment implements IRequestListener {



    private RecyclerView mRecyclerView;
    private DeviceTempState event;
    private DeviceInfo deviceInfo;
    private DeviceTempState changedDeviceTempState;
    private LinearLayout lSensorNotFound;
    private DeviceTempSensors deviceTempSensors;
    private int _countSensor;
    private FloatingActionButton fab;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_temperatura, container, false);
        mRecyclerView = view.findViewById(R.id.recycler_view_temp_state);
        lSensorNotFound = view.findViewById(R.id.layout_sensornotfound);
        lSensorNotFound.setVisibility(View.GONE);
        fab = view.findViewById(R.id.fab_temp);
        fab.bringToFront();
        fab.hide();
        fab.setOnClickListener(v -> OnFabClick());
        return view;
    }

  @Override
    public boolean onCheckValidResult() {
        if (!this.event.getTemp_timer_chanal().equals(changedDeviceTempState.getTemp_timer_chanal()))
            return false;
        if (!this.event.getTemp_timer_max_end().equals(changedDeviceTempState.getTemp_timer_max_end()))
            return false;
        if (!this.event.getTemp_timer_min_start().equals(changedDeviceTempState.getTemp_timer_min_start()))
            return false;
        return this.event.getTemp_timer_state().equals(changedDeviceTempState.getTemp_timer_state());
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceInfo deviceInfo) {
        if (deviceInfo != null) {
            this.deviceInfo = deviceInfo;
            if (changedDeviceTempState == null) {
                try {
                    CreateListAdapter();
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceTempSensors deviceTempSensors) {
        if (deviceInfo != null) {
            this.deviceTempSensors = deviceTempSensors;
            if (_countSensor != deviceTempSensors.getAvailableSensorsCount()) {
                _countSensor = deviceTempSensors.getAvailableSensorsCount();
                if (_countSensor > 0) {
                    lSensorNotFound.setVisibility(View.GONE);
                } else {
                    lSensorNotFound.setVisibility(View.VISIBLE);
                }
                if (changedDeviceTempState == null) {
                    try {
                        CreateListAdapter();
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceTempState event) {
        if (event != null) {
            this.event = event;
            if (changedDeviceTempState == null) {
                try {
                    CreateListAdapter();
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void CreateListAdapter() throws CloneNotSupportedException {
        if (this.event == null || this.deviceInfo == null || deviceTempSensors == null) return;
        if (snackbar != null)
            snackbar.dismiss();
        mRecyclerView.setAdapter(new TempStateAdapter(getContext(), event.clone(), this.deviceInfo, this.deviceTempSensors, e -> {
            if (getActivity() != null && !((DeviceActivity) getActivity()).isWait)
                fab.show();
            changedDeviceTempState = e;
        }));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return;
    }

    @Override
    public void OnFabClick() {
        if (!((DeviceActivity) Objects.requireNonNull(getActivity())).isWait) {
            ((DeviceActivity) getActivity()).isWait = true;
            snackbar.show();
            fab.hide();
            if (changedDeviceTempState != null) {
                new RequestManager().SendPOSTRequest(REQUEST_TYPE.REQUEST_DEVICE_TEMP_STATE, changedDeviceTempState, TemperaturaFragment.this);
            }
        } else {
            ToastHelper.ToastDeviceLog(getActivity(), RESULT_DATA_DEVICE_BUSY);
        }
    }

    @Override
    public void SendPostData() {
        new RequestManager().SendPOSTRequest(REQUEST_TYPE.REQUEST_DEVICE_TEMP_STATE, changedDeviceTempState, TemperaturaFragment.this);
    }

    @Override
    public void ClearTempData() {
        changedDeviceTempState = null;
    }

    @Override
    public void SetFabVisible(int visible) {

    }

}
