package com.aerosoft.aquacontroller.View.ViewAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.aerosoft.aquacontroller.Model.DataModel.ManagerStats;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.Controller.Chart.HelperLineChartView;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;


import java.util.List;

/**
 * Created by Doc on 14.10.2017.
 */

public class ChartTempAdapter extends ArrayAdapter {

    private final Context context;

    private List<List<Entry>> data;
    private final int[] ColorFade = {R.drawable.fade_list_green, R.drawable.fade_list_yellow, R.drawable.fade_list_green, R.drawable.fade_list_yellow};
    public ChartTempAdapter(Context context, List<List<Entry>> data) {
        super(context, 0, data);
        this.data = data;
        this.context = context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.fragment_temperatura_item, null);
            holder.chart = convertView.findViewById(R.id.lineStateTempChart);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        HelperLineChartView.Builder builder = new HelperLineChartView.Builder(new LineChart[]{holder.chart}, HelperLineChartView.ChartView.CHART_VIEW_LIST, context);

        try {
            builder
                    .SetChatLineDataSet(new List[]{data.get(position)}, new int[]{GetColor(position)}, position + 1)
                    .SetChatView()
                    .SetXAxis(ManagerStats.getInstance(context).GetXAxisValueForTemp(position))
                    .SetYAxis()
                    .Build();

        } catch (Exception e) {
            e.printStackTrace();
        }


        return convertView;
    }

    private class ViewHolder {

        LineChart chart;
    }

    private int GetColor(int position){
        if (position == 0) return ColorFade[0];
        if (position  == 1) return ColorFade[1];
        if (position  == 2) return ColorFade[2];
        if (position  == 3) return ColorFade[3];
        return ColorFade[1];
    }

}
