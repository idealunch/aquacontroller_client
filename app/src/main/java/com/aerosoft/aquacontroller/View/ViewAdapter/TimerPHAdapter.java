package com.aerosoft.aquacontroller.View.ViewAdapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.aerosoft.aquacontroller.Controller.DataListener.IDataPhTimerListener;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DevicePhTimers;
import com.aerosoft.aquacontroller.R;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TimerPHAdapter extends RecyclerView.Adapter<TimerPHAdapter.PhViewHolder> {

    private final DevicePhTimers phTimers;
    private final IDataPhTimerListener eventListner;
    private DeviceInfo deviceInfo;
    private Context mContext;

    private final int[] imagesID = {R.drawable.ic_numeric_1_box, R.drawable.ic_numeric_2_box, R.drawable.ic_numeric_3_box, R.drawable.ic_numeric_4_box, R.drawable.ic_numeric_5_box, R.drawable.ic_numeric_6_box, R.drawable.ic_numeric_7_box, R.drawable.ic_numeric_8_box};

    public TimerPHAdapter(Context mContext, DevicePhTimers phTimers, DeviceInfo deviceInfo, IDataPhTimerListener eventListner) {
        this.phTimers = phTimers;
        this.mContext = mContext;
        this.deviceInfo = deviceInfo;
        this.eventListner = eventListner;
    }

    @Override
    public TimerPHAdapter.PhViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new TimerPHAdapter.PhViewHolder(inflater.inflate(R.layout.ph_list_item, parent, false));
    }

    private boolean isSpinnerTouched = false;

    @Override
    public void onBindViewHolder(final TimerPHAdapter.PhViewHolder holder, final int position) {

        holder.timerStart.setText(String.format(Locale.ENGLISH, "%02.02f°", (float) (phTimers.getPh_start().get(position) * 10 + deviceInfo.getMin_ph()) / 100));
        holder.timerEnd.setText(String.format(Locale.ENGLISH, "%02.02f°", (float) (phTimers.getPh_end().get(position) * 10 + deviceInfo.getMin_ph()) / 100));
        holder.switchTimer.setChecked(phTimers.getPh_state().get(position) == 1);
        if (position % 2 != 0) {
            holder.view.setBackgroundColor(mContext.getResources().getColor(R.color.colorList1));
        } else {
            holder.view.setBackgroundColor(mContext.getResources().getColor(R.color.colorList2));
        }
        SpinnerAdapter customAdapter = new SpinnerAdapter(mContext, imagesID, deviceInfo.GetMaxCanal());
        holder.spChannel.setAdapter(customAdapter);
        holder.spChannel.setSelection(phTimers.getPh_canal().get(position));
        holder.spChannel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                isSpinnerTouched = true;
                return false;
            }
        });
        holder.spChannel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if (phTimers.getPh_canal().get(position) != pos) {
                    phTimers.getPh_canal().set(position, pos);
                    eventListner.onEvent(phTimers);
                    Log.d(getClass().getName().toString(), phTimers.getPh_canal().toString());
                    if (isSpinnerTouched) {
                        isSpinnerTouched = false;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (holder.controlsView.getVisibility() == View.GONE) {
            holder.chanalImage.setImageResource(imagesID[phTimers.getPh_canal().get(position)]);
        } else {
            holder.chanalImage.setImageResource(R.drawable.ic_up_arrow_angle);
        }


        holder.switchTimer.setChecked(phTimers.getPh_state().get(position) == 1);
        holder.switchTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phTimers.getPh_state().get(position) == 0)
                    phTimers.getPh_state().set(position, 1);
                else
                    phTimers.getPh_state().set(position, 0);

                if (phTimers.getPh_state().get(position) == 0) {
                    holder.timerEnable.setText(mContext.getString(R.string.OFF));
                } else {
                    holder.timerEnable.setText(mContext.getString(R.string.ON));
                }
                eventListner.onEvent(phTimers);
                Log.d(getClass().getName().toString(), phTimers.getPh_state().toString());
            }
        });
        if (phTimers.getPh_state().get(position) == 0) {
            holder.timerEnable.setText(mContext.getString(R.string.OFF));
        } else {
            holder.timerEnable.setText(mContext.getString(R.string.ON));
        }

        holder.timerStartChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog d = new Dialog(mContext);
                d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                d.setContentView(R.layout.number_dialog);
                ImageButton minus = d.findViewById(R.id.btnMinus);
                ImageButton plus = d.findViewById(R.id.btnPlus);
                final EditText tempText = d.findViewById(R.id.editTemp);
                tempText.setEnabled(false);
                final Button b1 = d.findViewById(R.id.button1);
                b1.setText(mContext.getString(R.string.button_cancel));
                TextView title = d.findViewById(R.id.dialog_title);
                title.setText(mContext.getString(R.string.dialog_title_temp));
                tempText.setText(String.format(Locale.ENGLISH, "%02.02f°",
                        (float) (phTimers.getPh_start().get(position) * 10 + deviceInfo.getMin_ph()) / 100));
                minus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        b1.setText(mContext.getString(R.string.button_ok));
                        Integer val = phTimers.getPh_start().get(position);
                        if (val > 0) {
                            val--;
                            phTimers.getPh_start().set(position, val);
                            tempText.setText(String.format(Locale.ENGLISH, "%02.02f°",
                                    (float) (phTimers.getPh_start().get(position) * 10 + deviceInfo.getMin_ph()) / 100));
                        }
                    }
                });

                plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        b1.setText(mContext.getString(R.string.button_ok));
                        Integer val = phTimers.getPh_start().get(position);
                        if (val < (deviceInfo.getMax_ph() - deviceInfo.getMin_ph()) / 10) {
                            val++;
                            phTimers.getPh_start().set(position, val);
                            tempText.setText(String.format(Locale.ENGLISH, "%05.02f°",
                                    (float) (phTimers.getPh_start().get(position) * 10 + deviceInfo.getMin_ph()) / 100));
                        }
                    }
                });

                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        holder.timerStart.setText(String.format(Locale.ENGLISH, "%02.02f°", (float) (phTimers.getPh_start().get(position) * 10 + deviceInfo.getMin_ph()) / 100));
                        eventListner.onEvent(phTimers);
                        Log.d(getClass().getName().toString(), phTimers.getPh_start().toString());
                        d.dismiss();

                    }
                });
                d.show();
            }
        });

        holder.timerEndChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog d = new Dialog(mContext);
                d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                d.setContentView(R.layout.number_dialog);
                ImageButton minus = d.findViewById(R.id.btnMinus);
                ImageButton plus = d.findViewById(R.id.btnPlus);
                final EditText tempText = d.findViewById(R.id.editTemp);
                tempText.setEnabled(false);
                final Button b1 = d.findViewById(R.id.button1);
                b1.setText(mContext.getString(R.string.button_cancel));
                TextView title = d.findViewById(R.id.dialog_title);
                title.setText(mContext.getString(R.string.dialog_title_temp));
                tempText.setText(String.format(Locale.ENGLISH, "%02.02f°",
                        (float) (phTimers.getPh_end().get(position) * 10 + deviceInfo.getMin_ph()) / 100));
                minus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        b1.setText(mContext.getString(R.string.button_ok));
                        Integer val = phTimers.getPh_end().get(position);
                        if (val > 0) {
                            val--;
                            phTimers.getPh_end().set(position, val);
                            tempText.setText(String.format(Locale.ENGLISH, "%02.02f°",
                                    (float) (phTimers.getPh_end().get(position) * 10 + deviceInfo.getMin_ph()) / 100));
                        }
                    }
                });

                plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        b1.setText(mContext.getString(R.string.button_ok));
                        Integer val = phTimers.getPh_end().get(position);
                        if (val < (deviceInfo.getMax_ph() - deviceInfo.getMin_ph()) / 10) {
                            val++;
                            phTimers.getPh_end().set(position, val);
                            tempText.setText(String.format(Locale.ENGLISH, "%02.02f°",
                                    (float) (phTimers.getPh_end().get(position) * 10 + deviceInfo.getMin_ph()) / 100));
                        }
                    }
                });

                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        holder.timerEnd.setText(String.format(Locale.ENGLISH, "%02.02f°", (float) (phTimers.getPh_end().get(position) * 10 + deviceInfo.getMin_ph()) / 100));
                        eventListner.onEvent(phTimers);
                        Log.d(getClass().getName().toString(), phTimers.getPh_end().toString());
                        d.dismiss();

                    }
                });
                d.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return phTimers.getPh_start().size();
    }

    class PhViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.controls_view)
        View controlsView;

        //group
        @BindView(R.id.group_timer_daily_chanal)
        ImageView chanalImage;
        @BindView(R.id.group_timer_daily_start)
        TextView timerStart;
        @BindView(R.id.group_timer_daily_end)
        TextView timerEnd;
        @BindView(R.id.group_timer_daily_enable)
        TextView timerEnable;
        //child
        @BindView(R.id.child_timer_start)
        ImageButton timerStartChild;
        @BindView(R.id.child_timer_end)
        ImageButton timerEndChild;
        @BindView(R.id.child_timer_swith)
        SwitchCompat switchTimer;

        @BindView(R.id.spinner_chanal_timer)
        Spinner spChannel;

        @BindView(R.id.timer_view)
        View view;

        PhViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.timer_view)
        void onTimerViewClicked() {
            if (controlsView.getVisibility() == View.VISIBLE) {
                controlsView.setVisibility(View.GONE);
                notifyDataSetChanged();
            } else {
                controlsView.setVisibility(View.VISIBLE);
            }
            if (controlsView.getVisibility() != View.GONE) {
                chanalImage.setImageResource(R.drawable.ic_up_arrow_angle);
            }
        }

    }
}

