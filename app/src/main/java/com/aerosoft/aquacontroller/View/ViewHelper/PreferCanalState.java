package com.aerosoft.aquacontroller.View.ViewHelper;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.aerosoft.aquacontroller.View.ViewHelper.PreferCanalState.TYPE_CANAL.CANAL_CO2_SYSTEMS;
import static com.aerosoft.aquacontroller.View.ViewHelper.PreferCanalState.TYPE_CANAL.CANAL_HEATERS;
import static com.aerosoft.aquacontroller.View.ViewHelper.PreferCanalState.TYPE_CANAL.CANAL_LIGHTS;
import static com.aerosoft.aquacontroller.View.ViewHelper.PreferCanalState.TYPE_CANAL.CANAL_WATER_FILTERS;

/**
 * Created by Vadim on 03.02.2017.
 */

public class PreferCanalState implements SharedPreferences.OnSharedPreferenceChangeListener{

    private static PreferCanalState mInstance;
    private Context context;

    private List<Integer> canalsLights;
    private List<Integer> canalsFilters;
    private List<Integer> canalsHeaters;
    private List<Integer> canalsCO2;

    private PreferCanalState() {
    }

    public static PreferCanalState getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new PreferCanalState(context);
        }
        return mInstance;
    }
    public PreferCanalState(Context context) {
        this.context = context;
        UpdateDataCanal();
    }
    private void UpdateDataCanal(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (sharedPreferences == null) return;
        SetNumberCanalType(CANAL_WATER_FILTERS, sharedPreferences.getStringSet("multi_select_list_preference_filter", null));
        SetNumberCanalType(CANAL_LIGHTS, sharedPreferences.getStringSet("multi_select_list_preference_light", null));
        SetNumberCanalType(CANAL_HEATERS, sharedPreferences.getStringSet("multi_select_list_preference_heater", null));
        SetNumberCanalType(CANAL_CO2_SYSTEMS, sharedPreferences.getStringSet("multi_select_list_preference_co2", null));
    }
    private void SetNumberCanalType(TYPE_CANAL type, Set<String> listCanal) {
        if (listCanal == null || listCanal.size() <= 0) return;
        List<Integer> canals = new ArrayList<>();
        for (String str : listCanal) {
            try {
                canals.add(Integer.parseInt(str));
            } catch (NumberFormatException e) {
                Log.e("PreferCanalState", e.getMessage());
            }
        }

        switch (type) {

            case UNDEFINED:
                break;
            case CANAL_LIGHTS:
                canalsLights = canals;
                break;
            case CANAL_WATER_FILTERS:
                canalsFilters = canals;
                break;
            case CANAL_HEATERS:
                canalsHeaters = canals;
                break;
            case CANAL_CO2_SYSTEMS:
                canalsCO2 = canals;
                break;
        }
    }

    @Nullable
    public List<Integer> getCanalsLights() {
        return canalsLights;
    }

    @Nullable
    public List<Integer> getCanalsFilters() {
        return canalsFilters;
    }

    @Nullable
    public List<Integer> getCanalsHeaters() {
        return canalsHeaters;
    }

    @Nullable
    public List<Integer> getCanalsCO2() {
        return canalsCO2;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        UpdateDataCanal();
    }

    public enum TYPE_CANAL {
        UNDEFINED,
        CANAL_LIGHTS,
        CANAL_WATER_FILTERS,
        CANAL_HEATERS,
        CANAL_CO2_SYSTEMS
    }
}
