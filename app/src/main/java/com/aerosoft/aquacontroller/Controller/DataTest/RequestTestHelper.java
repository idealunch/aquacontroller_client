package com.aerosoft.aquacontroller.Controller.DataTest;

import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Controller.DataHelper.UdpHelper;
import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.Model.TaskRequest.TaskGetRequest;
import com.aerosoft.aquacontroller.Model.TaskResponse.TaskResponse;

import org.greenrobot.eventbus.EventBus;

import java.util.EventObject;

import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult;

/**
 * Created by Vadim on 27.01.2017.
 * This is class used only for test application.
 */

public class RequestTestHelper {

    public static boolean IsTestRequest(ProtocolHelper.REQUEST_TYPE type, IRequestListener clientListener) {
        if (!UdpHelper.IS_TEST) return false;
        String json = "";
        switch (type) {

            case UNDEFINED:
                break;
            case REQUEST_DEVICE_INFO:
                json = "{\"status\": \"success\"," +
                        "\"message\": \"dev\"," +
                        "\"data\": {\n" +
                        "\"ver\": \"AQ_CH08WP\"," +
                        "\"m_t\": 10," +
                        "\"m_t_se\": 4," +
                        "\"min_t\": 1600," +
                        "\"max_t\": 3500" +
                        "}" +
                        "}";
                break;
            case REQUEST_DEVICE_TEMP_SENSOR:
                json = "{\n" +
                        "    \"status\": \"success\",\n" +
                        "    \"message\": \"t_sen\",\n" +
                        "    \"data\": {\n" +
                        "        \"t_se\": [0, 0, 0, 0]\n" +
                        "    }\n" +
                        "}";
                break;
            case REQUEST_DEVICE_CANAL_STATE:
                json = "{\n" +
                        "    \"status\": \"success\",\n" +
                        "    \"message\": \"c_s\",\n" +
                        "    \"data\": {\n" +
                        "        \"cl\": [2, 2, 2, 1, 1, 1, 1, 1],\n" +
                        "        \"c_t\": [2, 3, 3, 3, 3, 3, 3, 3]\n" +
                        "    }\n" +
                        "}";
                break;
            case REQUEST_DEVICE_TEMP_STATE:
                json = "{\n" +
                        "    \"status\": \"success\",\n" +
                        "    \"message\": \"te_s\",\n" +
                        "    \"data\": {\n" +
                        "        \"tt_s\": [0, 0, 0, 0],\n" +
                        "        \"tt_m_s\": [0, 0, 0, 0],\n" +
                        "        \"tt_m_e\": [76, 76, 76, 76],\n" +
                        "        \"tt_c\": [1, 0, 0, 0]\n" +
                        "    }\n" +
                        "}";
                break;
            case REQUEST_DEVICE_DAILY_TIMER_SATE:
                json = "{\n" +
                        "    \"status\": \"success\",\n" +
                        "    \"message\": \"td_s\",\n" +
                        "    \"data\": {\n" +
                        "        \"dt_h_s\": [0, 12, 12, 0, 0, 0, 0, 0, 0, 0],\n" +
                        "        \"dt_h_end\": [0, 20, 21, 0, 0, 0, 0, 0, 0, 0],\n" +
                        "        \"dt_m_s\": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],\n" +
                        "        \"dt_m_e\": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],\n" +
                        "        \"dt_s\": [0, 1, 1, 0, 0, 0, 0, 0, 0, 0],\n" +
                        "        \"dt_c\": [0, 1, 2, 0, 0, 0, 0, 0, 0, 0]\n" +
                        "    }\n" +
                        "}";
                break;
            case REQUEST_DEVICE_HOURS_TIMER_SATE:
                json = "{\n" +
                        "    \"status\": \"success\",\n" +
                        "    \"message\": \"th_s\",\n" +
                        "    \"data\": {\n" +
                        "        \"ht_m_st\": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],\n" +
                        "        \"ht_m_sp\": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],\n" +
                        "        \"ht_s\": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],\n" +
                        "        \"ht_c\": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]\n" +
                        "    }\n" +
                        "}";
                break;
            case REQUEST_DEVICE_SECOND_TIMER_SATE:
                json = "{\n" +
                        "    \"status\": \"success\",\n" +
                        "    \"message\": \"ts_s\",\n" +
                        "    \"data\": {\n" +
                        "        \"st_h_s\": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],\n" +
                        "        \"st_m_s\": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],\n" +
                        "        \"st_d\": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],\n" +
                        "        \"st_s\": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],\n" +
                        "        \"st_c\": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]\n" +
                        "    }\n" +
                        "}";
                break;
            case REQUEST_DEVICE_NTP_UPDATE:
                break;
            case REQUEST_DEVICE_LOG_INFO:
                break;
            case REQUEST_DEVICE_PH:
                json = "{\"status\":\"success\",\"message\":\"ph\",\"data\":{\"ph\":[25],\"stat\":[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]}}";
                break;
            case REQUEST_DEVICE_PH_TIMER:
                json = "{\n" +
                        "    \"status\": \"success\",\n" +
                        "    \"message\": \"ph_timer\",\n" +
                        "    \"data\": {\n" +
                        "        \"ph_s\": [0],\n" +
                        "        \"ph_e\": [60],\n" +
                        "        \"ph_st\": [0],\n" +
                        "        \"ph_c\": [1],\n" +
                        "        \"ph_401\": 844,\n" +
                        "        \"ph_686\": 797\n" +
                        "    }\n" +
                        "}";
                break;
            case REQUEST_DEVICE_TEMP_STATS:
                json = "{\"status\":\"success\",\"message\":\"temp_stats\",\"data\":{" +
                        "\"sensor0\":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,40]," +
                        "\"sensor1\":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,40]," +
                        "\"sensor2\":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,40]," +
                        "\"sensor3\":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,40]}}";
                break;
        }
        //1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24
        //0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
        TaskResponse<Cloneable> taskResult = new TaskGetRequest(type, null).TaskRequestComplete(json);
        if (taskResult != null) {
            EventBus.getDefault().postSticky(taskResult.getResponse());
            clientListener.onEvent(new EventObject(TaskResult.RESULT_SUCCESS));
        } else {
            clientListener.onEvent(new EventObject(ProtocolHelper.TaskResult.RESULT_DATA_CORRUPTED));
        }
        return true;
    }

}
