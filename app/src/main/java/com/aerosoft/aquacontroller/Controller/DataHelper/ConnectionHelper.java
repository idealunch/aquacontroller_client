package com.aerosoft.aquacontroller.Controller.DataHelper;

import android.os.Handler;

import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;

import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.*;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.EventObject;

/**
 * Created by Vadim on 21.11.2016.
 */

public class ConnectionHelper {

    private TaskResult result = TaskResult.RESULT_SUCCESS;
    final Handler myHandler = new Handler();

    public void getConnectedDevices(IRequestListener listner) {
        new ConnectedDevices(listner).start();
    }

    private class ConnectedDevices extends Thread {

        IRequestListener listner;

        public ConnectedDevices(IRequestListener listner) {
            this.listner = listner;
        }

        @Override
        public void run() {
            super.run();
            try {
                InetAddress currentPingAddr = InetAddress.getByName(UdpHelper.ADDRESS);
                NetworkInterface iFace = NetworkInterface.getByInetAddress(currentPingAddr);

                if (currentPingAddr.isReachable(iFace, 200, 1000)) {
                    result = TaskResult.RESULT_SUCCESS;
                    myHandler.post(ActionEventListener);
                    return;
                }
            } catch (UnknownHostException e) {
                result = TaskResult.RESULT_ERROR_UNKNOWN_HOST_EXCEPTION;
                myHandler.post(ActionEventListener);
                e.printStackTrace();
            } catch (IOException e) {
                result = TaskResult.RESULT_ERROR_IO_EXCEPTION;
                myHandler.post(ActionEventListener);
                e.printStackTrace();
            }
            result = TaskResult.RESULT_ERROR_NOT_RESPONSE;
            myHandler.post(ActionEventListener);
        }

        public TaskResult getResult() {
            if (UdpHelper.IS_TEST) return TaskResult.RESULT_SUCCESS;
            return result;
        }

        private Runnable ActionEventListener = new Runnable() {


            public void run() {
                if (listner != null) {
                    listner.onEvent(new EventObject(getResult()));
                }
            }
        };

    }
}
