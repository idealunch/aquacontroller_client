package com.aerosoft.aquacontroller.Controller.DataListener;

import com.aerosoft.aquacontroller.Model.DataModel.DeviceCanalState;

/**
 * Created by Vadim on 21.11.2016.
 * Interface for listener changed DataCanal in List Adapter
 */

public interface IDataCanalEventListener {
    void onEvent(DeviceCanalState e);
}
