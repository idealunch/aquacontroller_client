package com.aerosoft.aquacontroller.Controller.DataListener;

import com.aerosoft.aquacontroller.Model.DataModel.DevicePhTimers;

public interface IDataPhTimerListener {
    void onEvent(DevicePhTimers e);
}
