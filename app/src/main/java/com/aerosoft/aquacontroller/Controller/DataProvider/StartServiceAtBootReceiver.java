package com.aerosoft.aquacontroller.Controller.DataProvider;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.aerosoft.aquacontroller.Controller.DataHelper.UdpHelper;

/**
 * Created by Doc on 09.10.2017.
 */

public class StartServiceAtBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if("android.intent.action.BOOT_COMPLETED".equals(intent.getAction()))
        {
            if(!UdpHelper.IS_TEST){
            Intent serviceLauncher = new Intent(context, UDPBackGroundService.class);
            context.startService(serviceLauncher);}
        }
    }
}
