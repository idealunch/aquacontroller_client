package com.aerosoft.aquacontroller.Controller.Verification;

import com.aerosoft.aquacontroller.Model.DataModel.DeviceCanalState;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceHoursTimer;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceSecondsTimer;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempSensors;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempState;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTimer;
import com.aerosoft.aquacontroller.Model.TaskResponse.TaskResponse;

import java.util.ArrayList;

import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.CHANAL_AUTO;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.CHANAL_OFF;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.CHANAL_ON;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TIMER_OFF;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TIMER_ON;

/**
 * Created by Vadim on 12.12.2016.
 */

public class DataVerification {

    private DeviceInfo data;


    public DataVerification(DeviceInfo data) {
        if (VerificationDeviceInfo(data)) {
            this.data = data;

        }
    }

    public <T extends Cloneable> boolean VerificationTaskResponse(Class<T> type, TaskResponse<T> data) throws ExceptionInInitializerError {
        if (this.data == null) new ExceptionInInitializerError("Not Initialized");
        if (!TaskResponseVerification(data)) return false;
        if (type.isAssignableFrom(DeviceInfo.class)) {
            VerificationDeviceInfo((DeviceInfo) data.getResponse());
        } else if (type.isAssignableFrom(DeviceCanalState.class)) {
            VerificationDeviceChanalState((DeviceCanalState) data.getResponse());
        } else if (type.isAssignableFrom(DeviceTimer.class)) {
            VerificationDeviceTimer((DeviceTimer) data.getResponse());
        } else if (type.isAssignableFrom(DeviceHoursTimer.class)) {
            VerificationDeviceHoursTimer((DeviceHoursTimer) data.getResponse());
        } else if (type.isAssignableFrom(DeviceSecondsTimer.class)) {
            VerificationDeviceSecondsTimer((DeviceSecondsTimer) data.getResponse());
        } else if (type.isAssignableFrom(DeviceTempState.class)) {
            VerificationDeviceTempState((DeviceTempState) data.getResponse());
        } else if (type.isAssignableFrom(DeviceTempSensors.class)) {
            VerificationDeviceTempSensors((DeviceTempSensors) data.getResponse());
        }
        return true;
    }

    private boolean VerificationDeviceInfo(DeviceInfo data) {
        if (data.getMax_temp() <= 0 || data.getMin_temp() <= 0 && data.getMax_timer() <= 0 ||
                data.getMax_temp_sensor() <= 0 || data.getVersion().length() <= 0) return false;
        return data.GetMaxCanal() != -1;
    }

    private boolean VerificationDeviceChanalState(DeviceCanalState data) {
        if (!StateChanalVerification(data.getCanal())) return false;
        return StateSetChanalVerification(data.getCanal_timer());
    }

    private boolean VerificationDeviceTimer(DeviceTimer data) {
        if (!HourVerification(data.getDaily_timer_hour_start())) return false;
        if (!HourVerification(data.getDaily_timer_hour_end())) return false;
        if (!MinVerification(data.getDaily_timer_min_start())) return false;
        if (!MinVerification(data.getDaily_timer_min_end())) return false;
        if (!StateTimerVerification(data.getDaily_timer_state())) return false;
        return ChanalVerification(data.getDaily_timer_canal());
    }

    private boolean VerificationDeviceHoursTimer(DeviceHoursTimer data) {
        if (!MinVerification(data.getHours_timer_min_start())) return false;
        if (!MinVerification(data.getHours_timer_min_stop())) return false;
        if (!StateTimerVerification(data.getHours_timer_state())) return false;
        return ChanalVerification(data.getHours_timer_canal());
    }

    private boolean VerificationDeviceSecondsTimer(DeviceSecondsTimer data) {
        if (!HourVerification(data.getSecond_timer_hour_start())) return false;
        if (!MinVerification(data.getSecond_timer_min_start())) return false;
        if (!SecVerification(data.getSecond_timer_duration())) return false;
        if (!StateTimerVerification(data.getSecond_timer_state())) return false;
        return ChanalVerification(data.getSecond_timer_canal());
    }

    private boolean VerificationDeviceTempState(DeviceTempState data) {
        if (!TempVerification(data.getTemp_timer_min_start())) return false;
        if (!TempVerification(data.getTemp_timer_max_end())) return false;
        if (!StateTimerVerification(data.getTemp_timer_state())) return false;
        return ChanalVerification(data.getTemp_timer_chanal());
    }

    private boolean VerificationDeviceTempSensors(DeviceTempSensors data) {
        return TempVerification(data.getSensors());
    }

    private boolean HourVerification(ArrayList<Integer> hours) {
        if (hours.size() != this.data.getMax_timer()) return false;
        for (int i = 0; i < hours.size(); i++) {
            if (hours.get(i) < 0 || hours.get(i) > 23)
                return false;
        }
        return true;
    }

    private boolean MinVerification(ArrayList<Integer> min) {
        if (min.size() != this.data.getMax_timer()) return false;
        for (int i = 0; i < min.size(); i++) {
            if (min.get(i) < 0 || min.get(i) > 59)
                return false;
        }
        return true;
    }

    private boolean SecVerification(ArrayList<Integer> seconds) {
        if (seconds.size() != this.data.getMax_timer()) return false;
        for (int i = 0; i < seconds.size(); i++) {
            if (seconds.get(i) < 0 || seconds.get(i) > 999)
                return false;
        }
        return true;
    }

    private boolean StateSetChanalVerification(ArrayList<Integer> state) {
        if (state.size() != this.data.GetMaxCanal()) return false;
        if (state.size() != data.getMax_timer()) return false;
        for (int i = 0; i < state.size(); i++) {
            if (state.get(i) < CHANAL_OFF || state.get(i) > CHANAL_AUTO)
                return false;
        }
        return true;
    }

    private boolean StateChanalVerification(ArrayList<Integer> state) {
        if (state.size() != this.data.GetMaxCanal()) return false;
        for (int i = 0; i < state.size(); i++) {
            if (state.get(i) < CHANAL_OFF || state.get(i) > CHANAL_ON)
                return false;
        }
        return true;
    }

    private boolean StateTimerVerification(ArrayList<Integer> state) {
        if (state.size() != this.data.getMax_timer()) return false;
        for (int i = 0; i < state.size(); i++) {
            if (state.get(i) < TIMER_OFF || state.get(i) > TIMER_ON)
                return false;
        }
        return true;
    }

    private boolean TempVerification(ArrayList<Integer> temp) {
        if (temp.size() != this.data.getMax_temp_sensor()) return false;
        for (int i = 0; i < temp.size(); i++) {
            if (temp.get(i) < this.data.getMin_temp() || temp.get(i) > this.data.getMax_temp())
                return false;
        }
        return true;
    }

    private boolean ChanalVerification(ArrayList<Integer> temp) {
        if (temp.size() != this.data.getMax_timer()) return false;
        for (int i = 0; i < temp.size(); i++) {
            if (temp.get(i) < 0 || temp.get(i) > this.data.GetMaxCanal())
                return false;
        }
        return true;
    }

    private boolean TaskResponseVerification(TaskResponse data) {
        if (data.getStatus() == null || data.getStatus().length() <= 0) return false;
        return !(data.getMessage() == null || data.getMessage().length() <= 0);
    }

}
