package com.aerosoft.aquacontroller.Controller.DataProvider;


import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.aerosoft.aquacontroller.Controller.DataHelper.Constants;
import com.aerosoft.aquacontroller.Controller.DataHelper.GsonHelper;
import com.aerosoft.aquacontroller.Controller.DataHelper.UdpHelper;
import com.aerosoft.aquacontroller.Controller.DataListener.IRequestPnPListener;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceCanalState;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DevicePh;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempSensors;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.Fragments.Preference.ShareManager;
import com.aerosoft.aquacontroller.View.SplashActivity;
import com.aerosoft.aquacontroller.View.ViewHelper.Helpers;
import com.crashlytics.android.Crashlytics;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.EventObject;
import java.util.Locale;

import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_DEVICE_CANAL_SETTINGS;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_DEVICE_INFO;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_DEVICE_PH;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_DEVICE_TEMP_SENSOR;

/**
 * Created by Vadim on 03.02.2017.
 */


public class UDPBackGroundService extends Service {

    public static final int DEFAULT_NOTIFICATION_ID = 101;

    private IRequestPnPListener mOnEventListener;
    private UdpServerThread udpServerThread;
    private NotificationManager notificationManager;
    private DeviceInfo deviceInfo;
    private DeviceTempSensors deviceTempSensor;
    private DeviceCanalState deviceCanalState;
    private DevicePh devicePhState;
    private AlarmManager alarmManager;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {


    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        deviceTempSensor = null;
        deviceCanalState = null;
        deviceInfo = null;

        notificationManager = (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);

        ShareManager.getInstance(this).Initilized();
        DoTaskService();
        return START_REDELIVER_INTENT;
    }

    /**
     * Start Service work
     */
    private void DoTaskService() {

        try {
            sendNotification("");
        } catch (Exception e) {
            Log.d(this.getClass().getName(), e.getMessage());

        }
        if (udpServerThread == null) {
            //  Fabric.with(this, new Crashlytics());
            Log.d(this.getClass().getName(), "Create UDP Server!");
            udpServerThread = new UdpServerThread(new IRequestPnPListener() {

                @Override
                public void onEvent(EventObject e, EventObject e1) {
                    try {
                        GsonHelper.ResultTaskResponse<Cloneable> result = GsonHelper.JSONtoObject(e.getSource().toString());
                        if (result != null) {
                            SendEvent(result, e1.getSource().toString());
                        }
                        sendNotification("");
                    } catch (Exception exp) {
                        Log.d(this.getClass().getName(), exp.getMessage());
                        Crashlytics.log(exp.getMessage());
                    }
                }
            });
        }
        if (!udpServerThread.isAlive()) {
            Log.d(this.getClass().getName(), "Start UDP Server!");
            udpServerThread.Start();
        }
    }

    /**
     * Send event to Application
     *
     * @param result
     */
    private void SendEvent(final GsonHelper.ResultTaskResponse<Cloneable> result, String IP) {
        new Handler(Looper.getMainLooper()).post(() -> {

            if (result == null) return;
            if (result.GetTypeMessage().equals(REQUEST_DEVICE_INFO)) {
                if (IP.length() > 0 && Constants.PATTERN.matcher(IP).matches()) {
                    if(ShareManager.getInstance(getApplicationContext()).AddIPDevices(IP)){
                        EventBus.getDefault().postSticky(result.GetResult().getResponse());
                    }
                }
            }
            if (UdpHelper.ADDRESS != IP) return;
            if (result.GetTypeMessage().equals(REQUEST_DEVICE_TEMP_SENSOR)) {
                deviceTempSensor = (DeviceTempSensors) result.GetResult().getResponse();
            } else if (result.GetTypeMessage().equals(REQUEST_DEVICE_CANAL_SETTINGS)) {
                deviceCanalState = (DeviceCanalState) result.GetResult().getResponse();
            } else if (result.GetTypeMessage().equals(REQUEST_DEVICE_PH)) {
                devicePhState = (DevicePh) result.GetResult().getResponse();
            } else if (result.GetTypeMessage().equals(REQUEST_DEVICE_INFO)) {
                if (deviceInfo != null) {
                    deviceInfo = (DeviceInfo) result.GetResult().getResponse();
                }
            }
            EventBus.getDefault().postSticky(result.GetResult().getResponse());

        });
    }

    /**
     * UDP Server listener
     */
    private class UdpServerThread {

        private MyDatagramReceiver myDatagramReceiver;
        private Runnable ActionEventListener = new Runnable() {
            public void run() {
                if (myDatagramReceiver == null) return;
                if (mOnEventListener != null)
                    mOnEventListener.onEvent(new EventObject(myDatagramReceiver.getLastMessage()), new EventObject(myDatagramReceiver.getLastIP()));
            }
        };

        UdpServerThread(IRequestPnPListener listener) {
            mOnEventListener = listener;
        }

        void Start() {
            myDatagramReceiver = new MyDatagramReceiver();
            myDatagramReceiver.start();
        }

        boolean isAlive() {
            return myDatagramReceiver != null && myDatagramReceiver.isAlive();
        }

        public void onResume() {
            Start();
        }

        public void onPause() {
            myDatagramReceiver.kill();
        }

        private class MyDatagramReceiver extends Thread {
            final Handler myHandler = new Handler();
            private boolean bKeepRunning = true;
            private String lastMessage = "";
            private String IP = "";

            public void run() {
                String message;
                byte[] lmessage = new byte[UdpHelper.MAX_UDP_DATAGRAM_LEN];
                DatagramPacket packet = new DatagramPacket(lmessage, lmessage.length);

                try {
                    DatagramSocket socket = new DatagramSocket(UdpHelper.PORT);

                    while (bKeepRunning) {
                        socket.receive(packet);
                        message = new String(lmessage, 0, packet.getLength());
                        lastMessage = message;
                        IP = packet.getAddress().getHostAddress();
                        Log.d("MyDatagramReceiver ", message);
                        myHandler.post(ActionEventListener);
                    }

                    socket.close();
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }

            private void kill() {
                bKeepRunning = false;
            }

            @org.jetbrains.annotations.Contract(pure = true)
            private String getLastMessage() {
                return lastMessage;
            }

            private String getLastIP() {
                return IP;
            }
        }
    }

    /**
     * Send custom notification
     *
     * @param Ticker
     */

    public void sendNotification(String Ticker) {

        //These three lines makes Notification to open main activity after clicking on it
        Intent notificationIntent = new Intent(this, SplashActivity.class);
        notificationIntent.setAction(Intent.ACTION_MAIN);
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        String channelId = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = createNotificationChannel();
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId);
        builder.setContentIntent(contentIntent).setOngoing(true).setSmallIcon(R.drawable.ic_fish_action_bar);
        if (deviceInfo != null && deviceTempSensor != null && deviceTempSensor.getSensors() != null) {
            builder.setLargeIcon(Helpers.textAsBitmap(String.format(Locale.ENGLISH, "%.0f", (float) (deviceTempSensor.getSensors().get(0) * 25 + deviceInfo.getMin_temp()) / 100) + "°", 80, Color.BLACK));
        } else {
            builder.setLargeIcon(Helpers.textAsBitmap("--°", 80, Color.BLACK));
        }
        builder.setTicker(Ticker);
        builder.setContentTitle(getString(R.string.app_name)); //Заголовок
        if (deviceCanalState != null) {
            String StateCanalText = "";
            for (int i = 1; i < deviceCanalState.getCanal().size() + 1; i++) {
                if (deviceCanalState.getCanal().get(i - 1) == 1) StateCanalText += "_";
                else
                    StateCanalText += i;
            }
            builder.setContentText("Canal: <" + StateCanalText + "> " + UdpHelper.ADDRESS);
            Log.d("MyDatagramReceiver ", StateCanalText);
        } else {
            builder.setContentText("Wait for update " + UdpHelper.ADDRESS);
        }
        builder.setWhen(System.currentTimeMillis());
        Notification notification;
        notification = builder.build();
        startForeground(DEFAULT_NOTIFICATION_ID, notification);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String createNotificationChannel() {
        String channelId = "AquaController";
        String channelName = "Background Service";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_NONE);
        channel.setLightColor(Color.BLUE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }

    @Override
    public void onDestroy() {
        udpServerThread = null;
        super.onDestroy();

    }

}

class JsonTask extends AsyncTask<String, String, GsonHelper.ResultTaskResponse<Cloneable>> {

    protected GsonHelper.ResultTaskResponse<Cloneable> doInBackground(String... params) {


        HttpURLConnection connection = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(params[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();


            InputStream stream = connection.getInputStream();

            reader = new BufferedReader(new InputStreamReader(stream));

            StringBuffer buffer = new StringBuffer();
            String line = "";

            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");
                Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)

            }
            GsonHelper.ResultTaskResponse<Cloneable> taskResponse = GsonHelper.JSONtoObject(buffer.toString());
            return taskResponse;


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


}