package com.aerosoft.aquacontroller.Controller.DataListener;

import java.util.EventObject;

/**
 * Created by Vadim on 11.11.2016.
 * Interface for listener changed  UDP request
 */

public interface IRequestListener {
    void onEvent(EventObject e);
}
